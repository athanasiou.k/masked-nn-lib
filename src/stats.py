import json
data = {}

with open("unshared.out") as f:
    for line in f:
        entries=line.split()
        nn=""
        nn_type=""
        nn_acc=0
        for e in entries:
            key, val = e.split(":")
            if key == "nn":
                nn = val
                if nn not in data:
                    data[nn] = {}
            elif key == "type":
                nn_type = val
                if nn_type not in data[nn]:
                    data[nn][nn_type] = {}
            elif key == "accuracy":
                nn_acc =float(val)
                data[nn][nn_type]["accuracy"] = nn_acc
            elif key == "avgcycles":
                nn_cc = int(val)
                data[nn][nn_type]["avgcycles"] = nn_cc

with open("shared.out") as f:
    for line in f:
        entries=line.split()
        nn=""
        nn_type=""
        nn_acc=0
        for e in entries:
            key, val = e.split(":")
            if key == "nn":
                nn = val
            elif key == "type":
                nn_type = val
                if nn_type not in data[nn]:
                    data[nn][nn_type] = {}
            elif key == "accuracy":
                nn_acc =float(val)
                data[nn][nn_type]["accuracy"] = nn_acc
            elif key == "avgcycles":
                nn_cc = int(val)
                data[nn][nn_type]["avgcycles"] = nn_cc
                
for nn in data:
    baseline = data[nn]["UNSHARED"]["avgcycles"]
    sharedprngoff = data[nn]["PRNG_OFF-ORIGINAL"]["avgcycles"]
    data[nn]["PRNG_OFF-ORIGINAL"]["overhead"] = sharedprngoff / baseline
    sharedoriginal = data[nn]["PRNG_ON-ORIGINAL"]["avgcycles"]
    data[nn]["PRNG_ON-ORIGINAL"]["overhead"] = sharedoriginal / baseline
    sharedtightened = data[nn]["PRNG_ON-TIGHTENED"]["avgcycles"]
    data[nn]["PRNG_ON-TIGHTENED"]["overhead"] = sharedtightened / baseline


print("--- JSON format ---\n")
print(json.dumps(data, indent=4))

print("\n--- LaTeX Table Format ---\n")
print("\\begin{tabular}{llrrrrrrr}\\toprule")
print("DNN & Architecture & \multicolumn{2}{c}{UNSHARED} & \multicolumn{1}{c}{SHAREDOFF} & \multicolumn{2}{c}{SHAREDON} &\multicolumn{2}{c}{TIGHTENED}\\")
print("&  & Acc. \% & Cycles & Cycles & Acc. $\delta$ & Cycles & Acc. $\delta$ & Cycles\\")
print("\midrule")

def add_commas(n):
    return "{:,}".format(n)
for nn in data:
    if "mlp" in nn:
        dnn = "MLP"
        arch = nn[3:]
    elif "bnn" in nn:
        dnn = "BNN"
        arch = nn[3:]
    elif "cnn" in nn:
        dnn = "CNN"
        arch = "(16,5)-(32,5)-1568"
    elif "lenet" in nn:
        dnn = "CNN"
        arch = "(6,5)-(16,5)-256-120-84"
    else:
        print("wrong nn type")
        break
    print(dnn, "& ", end='')
    print(arch, "& ", end='')
    print(data[nn]["UNSHARED"]["accuracy"], "& ", end='')
    print(add_commas(data[nn]["UNSHARED"]["avgcycles"]), "& ", end='')
    print(add_commas(data[nn]["PRNG_OFF-ORIGINAL"]["avgcycles"]), "& ", end='')
    print(round(data[nn]["PRNG_ON-ORIGINAL"]["accuracy"]-
                data[nn]["UNSHARED"]["accuracy"],3), "& ", end='')
    print(add_commas(data[nn]["PRNG_ON-ORIGINAL"]["avgcycles"]), "& ", end='')
    print(round(data[nn]["PRNG_ON-TIGHTENED"]["accuracy"]-
                data[nn]["UNSHARED"]["accuracy"], 3), "& ", end='')
    print(add_commas(data[nn]["PRNG_ON-TIGHTENED"]["avgcycles"]), "\\\\")

print("\\bottomrule")
print("\end{tabular}")
