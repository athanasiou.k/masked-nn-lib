#ifndef _KeccakRNG_h_
#define _KeccakRNG_h_

typedef unsigned int u32;
typedef unsigned char u8;

typedef unsigned int UINT32;

void KeccakF( UINT32 * state, const UINT32 *in, int laneCount );

unsigned char RetRand();
void reset_Keccak();

void InitRand();
void myInitRand(unsigned int *rand_state);

extern int Keccak_inv_cnt;

#endif
