#include <x86intrin.h>

static inline unsigned long long get_cycles()
{
	unsigned dummy = 0;
	unsigned long long t;
	_mm_lfence();
	_mm_mfence();
	t = __rdtscp(&dummy);
	_mm_lfence();
	_mm_mfence();
	return t;
  
}
