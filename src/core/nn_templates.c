#ifdef T

#include "templates.h"
#include "ops.h"
#include <stdio.h>

void TEMPLATE(relu,T)(T* v, int size)
{
	T sign[size], sign_n[size], zero[1];
	
	MVI(zero, 0);
	
	for (int i = 0; i < size; i++)
	{
		CMP((sign+i), (sign_n+i), (v+i), zero);
		MUL((v+i), (sign+i), (v+i));
		L-=5;
	}
	L+=5;

}


void TEMPLATE(trunc,T)(T* v, int size)
{
	for (int i = 0; i < size; i++)
		TRN(v+i);
}



void TEMPLATE(add_bias,T)(T *a, int a_rows, int a_cols, T *bias)
{
	for (int i = 0; i < a_rows; i++)
		for (int j = 0; j < a_cols; j++)
			ADD(a + i*a_cols + j, bias, a + i*a_cols + j);
}

void TEMPLATE(argmax,T)(T* v, int size, T* max, T* arg)
{
	
	T gt[1], gt_n[1], tmp1[1], tmp2[1], c[1];
	
	ASG(max, v);
	MVI(arg, 0);
	MVI(c, 0);
	for (int i = 0; i < size; i++)
	{
		CMP(gt, gt_n, v+i, max);

		MUL(tmp1, v+i, gt);

		MUL(tmp2, max, gt_n);

		ADD(max, tmp1, tmp2);

		MVI(c, i);

		MUL(tmp1, c, gt);

		MUL(tmp2, arg, gt_n);
		ADD(arg, tmp1, tmp2);
	}

}


void TEMPLATE(maxpool,T)(T *f, int f_rows, int f_cols, int k_dim, T *d, int d_rows, int d_cols)
{

	T max[1];
	T gt[1];
	T gt_n[1];
	T tmp1[1], tmp2[1];
	for (int r = 0; r < d_rows; r++)
		for (int c = 0; c < d_cols; c++)
		{
			ASG(max, f+r*k_dim*d_cols*k_dim + c*k_dim);
			for (int k_row = 0; k_row < k_dim; k_row++)
				for (int k_col = 0; k_col < k_dim; k_col++)
				{
					CMP(gt, gt_n, f+(r*k_dim*d_cols*k_dim + k_row*d_cols*k_dim) + (c*k_dim+k_col), max);
					MUL(tmp1, f+(r*k_dim*d_cols*k_dim + k_row*d_cols*k_dim) + (c*k_dim+k_col), gt);
					MUL(tmp2, max, gt_n);
					ADD(max, tmp1, tmp2);
				}
			L -= (6*sq(k_dim));
			ASG(d + r*d_cols+c, max);
		}

}


void TEMPLATE(maxpoollayer,T)(T *in, int in_channels, int in_dim,
			      int k_dim, int stride, int padding, T *out)
{
	
	int out_dim = (in_dim - k_dim + 2*padding)/stride +1;
	
	for (int c = 0; c < in_channels; c++)
	{
		TEMPLATE(maxpool, T)(in + c*sq(in_dim), in_dim, in_dim, k_dim,
					  out + c*sq(out_dim), out_dim, out_dim);
	}
	L += 6*sq(k_dim);

}

// this assumes stride of 1.
int TEMPLATE(conv2d,T)(T *ic, int ic_rows, int ic_cols,
		       T *kernel, int k_dim, int stride, int padding, 
		       T *oc, int oc_rows, int oc_cols)
{
	if ((ic_rows != ic_cols) || (oc_rows != oc_cols))
		printf("Warning: Input and/or Output row-column mismatch\n");
	
	if (oc_rows != ((ic_rows - k_dim + 2*padding)/stride +1))
	{
		printf("Error: %d != (%d - %d + 2*%d)/%d +1\n", oc_rows, ic_rows, k_dim, padding, stride);
		return 1;
	}
	
	T sum[1];
	/* T tmp[1]; */
	int s = -(k_dim)/2;
	int e = k_dim/2 + k_dim%2;
	for (int r = 0; r < oc_rows; r++)
		for (int c = 0; c < oc_cols; c++)
		{
			int ir_center = r + (k_dim/2)-padding;
			int ic_center = c + (k_dim/2)-padding;
			MVI(sum, 0);
			CONV(sum, ic, kernel, ir_center, ic_center, s, e, ic_rows, ic_cols, k_dim);
			ADD(oc + r*oc_cols+ c, sum, oc + r*oc_cols+ c);
		}
	
	
	return 0;
}

void TEMPLATE(convlayer,T)(T *in, int in_channels, int in_dim,
			   T *weights, int kernel_dim, int stride, int padding,
			   T *out, int out_channels, T *bias)
{
	
	int out_dim = (in_dim - kernel_dim + 2*padding)/stride +1;

	ZERO(out, sq(out_dim)*out_channels);
	for (int out_chan = 0; out_chan < out_channels; out_chan++)
	{
		for (int in_chan = 0; in_chan < in_channels; in_chan++)
		{

			TEMPLATE(conv2d, T)(in + in_chan*sq(in_dim), in_dim, in_dim,
					    weights + out_chan*in_channels*sq(kernel_dim) + in_chan*sq(kernel_dim),
					    kernel_dim, stride, padding,
					    out + out_chan*sq(out_dim), out_dim, out_dim);

		}
		L++;
		TEMPLATE(trunc, T)(out+out_chan*sq(out_dim), sq(out_dim));
		L++;
		TEMPLATE(add_bias, T)(out+out_chan*sq(out_dim), out_dim, out_dim, bias+out_chan);
		L-=2;
	}
	L+=2;

}


void TEMPLATE(dense,T)(T *in, int in_dim, T *w, int out_dim, T *out, T *bias, int activation)
{

	int i;
	T sign[out_dim], sign_n[out_dim], zero[1];
	
	MVI(zero, 0);

	for (i = 0; i < out_dim; i++)
	{

		DOT((out+i), (w + i*in_dim), in,  in_dim);
		L++;

		TRN((out+i));
		L++;

		ADD((out+i), (out+i), (bias+i));
		
		if (!activation)
		{
			L-=2;
			continue;
		}
		

		CMP((sign+i), (sign_n+i), (out+i), zero);

		MUL((out+i), (sign+i), (out+i));
		L-=7;
	}

	if (!activation)
		L+=2;
	else
		L+=7;
	
}



void TEMPLATE(print_cnn_layer,T)(T *l, int channels, int dim, int kernel_dim)
{
	for (int o=0; o < channels; o++)
	{
		printf("channel %d\n", o);
		for (int i=0; i < dim; i++)
		{
			printf("{\t");
			for (int j=0; j < dim; j++)
			{
				PRINT(l + o*sq(dim) +i*dim +j);
				printf(", ");
			}
			printf("}\n");
		}

	}

}

void TEMPLATE(print_fnn_layer,T)(T *l, int dim)
{
	for (int i=0; i < dim; i++)
	{
		PRINT(l+i);
		printf("\n");
	}
}


#endif
