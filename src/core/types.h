#ifndef TYPES_H_
#define TYPES_H_
#include <stdint.h>
#define D 2
typedef int32_t fixed_point_t;
typedef uint32_t ring_t;
typedef struct share { ring_t s[D];} share;


#define FP_FRACTIONAL_BITS 6
#define RING_BITS 32
#define RING_MAX_NUM (- 1)
#define FP_RING_BITS RING_BITS
#ifdef LX
#define FP_SAFE_BITS LX
#else
#define FP_SAFE_BITS 16
#endif
#define FP_SAFE_UB_NUM 1<<FP_SAFE_BITS
#define FP_SAFE_LB_NUM RING_MAX_NUM - ( 1<<FP_SAFE_BITS )
#ifdef LD
#define FP_FRACTIONAL_BITS LD
#else
#define FP_FRACTIONAL_BITS 6
#endif
// Not for masked fixed-point model
#define FP_DECIMAL_BITS (FP_RING_BITS - FP_FRACTIONAL_BITS)
#define FP_MIN_NUM (-(RING_MAX_NUM-1))
#endif
