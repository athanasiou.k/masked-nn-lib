extern int paddc;
extern int pmulc;
extern int pxorc;
extern int pandc;
extern int prshc;
extern int pcmpc;
extern int pnegc;
#ifdef PCNT
#define PADD(x, y) ( (x) + (y) ); paddc++;
#define PMUL(x, y) ( (x) * (y)  ); pmulc++;
#define PXOR(x, y) ( (x) ^ (y) ); pxorc++;
#define PRSHIFT(x, y) ( (x) >> (y) ); prshc++;
#define PAND(x, y) ( (x) & (y) ); pandc++;
#define PGT(x, y) ( (x) > (y) ); pcmpc++;
#define PLE(x, y) ( (x) <= (y) ); pcmpc++;
#define PNEG(x) ( (x) ^ 1 ); pnegc++;
#define PLSHIFT(x, y) ( (x) << (y) ); prshc++;
#else
#define PADD(x, y) ( (x) + (y) );
#define PMUL(x, y) ( (x) * (y)  );
#define PXOR(x, y) ( (x) ^ (y) );
#define PRSHIFT(x, y) ( (x) >> (y) );
#define PAND(x, y) ( (x) & (y) );
#define PGT(x, y) ( (x) > (y) );
#define PLE(x, y) ( (x) <= (y) );
#define PNEG(x) ( (x) ^ 1 );
#define PLSHIFT(x, y) ( (x) << (y) );
#endif
