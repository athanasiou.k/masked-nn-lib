#ifndef NN_H_
#define NN_H_
#include <stdint.h>
#include <math.h>
#include <stddef.h>
#include <stdio.h>

#include "KeccakRNG.h"

#include "prim_ops.h"
#include "utils.h"
#include "types.h"

#include "templates.h"

#ifdef T
#undef T
#endif
#ifdef S
#undef S
#endif
#define S 1
#define T double
#include "nn_templates.h"

#ifdef T
#undef T
#endif
#ifdef S
#undef S
#endif
#define S 1
#define T fixed_point_t
#include "nn_templates.h"

#ifdef T
#undef T
#endif
#define T share
#ifdef S
#undef S
#endif
#define S 2
#include "nn_templates.h"

// UTILS
static inline int sq(int x) {return x*x;};

static inline fixed_point_t float_to_fixed(double a)
{
	float z = a * (1 << FP_FRACTIONAL_BITS);
	fixed_point_t res = (fixed_point_t)round(z);
	return res;
}

static inline double fixed_to_float(fixed_point_t a)
{
	double res = (double)a;
	res = res /(double)(1 << FP_FRACTIONAL_BITS);
	return res;
}


static inline void float_to_fixed_a(double *a, int dim, fixed_point_t *b)
{
    	for(int i = 0; i < dim; i++)
		b[i] = float_to_fixed(a[i]);
}

// MASKING UTILS
void mask_a(fixed_point_t *a, int size, share *a_m);
static inline void reconst(ring_t *a, share *b)
{
	for (int i = 0; i < D; i++)
		(*a)  += b->s[i];
}


// OVERLOADED OPS
static inline void print_d(double *d)
{ printf("%.4f", d[0]); }

static inline void print_f(fixed_point_t *f)

{ printf("%u = %.4f", (ring_t)f[0], fixed_to_float(f[0])); }

static inline void print_r(share *r)
{
	ring_t n = 0;
	reconst(&n, r);
	printf("%u", n);
}


static inline void mul_d(double *c, double *a, double *b)
{ (*c) =  (*a) * (*b); }

static inline void mul_f(fixed_point_t *c, fixed_point_t *a, fixed_point_t *b)
{ *c =  (*a) * (*b); }

void masked_mul(share *c, share *a, share *b);
void masked_mul2d(share *c, share *a, share *b);

static inline void dotprod_d(double *c, double *a, double *b, int n)
{
	(*c) = 0;
	for (int k = 0; k<n; k++)
		(*c) += a[k]*b[k];

}


static inline void dotprod_f(fixed_point_t *c, fixed_point_t *a, fixed_point_t *b, int n)
{
	(*c) = 0;
	fixed_point_t tmp;
	for (int k = 0; k<n; k++)
	{
		tmp = PMUL(a[k], b[k]);
		(*c) = PADD((*c), tmp);
	}
}

static inline void conv_f(fixed_point_t *oc, fixed_point_t *ic, fixed_point_t *kernel,
			  int r, int c, int s, int e, int ic_rows, int ic_cols, int k_dim)
{
	int ic_idx, kr_idx;
	fixed_point_t tmp;
	for (int k_row = s; k_row < e; k_row++)
	{
		for (int k_col = s; k_col < e; k_col++)
		{
			
			if ((r + k_row < 0) || (r + k_row >= ic_rows) ||
			    (c + k_col < 0) || (c + k_col >= ic_cols))
				continue;
			
			ic_idx = (r+k_row)*ic_cols + (c+k_col);
			kr_idx = (k_row+(k_dim/2))*k_dim + (k_col+(k_dim/2));
			
			tmp = PMUL(ic[ic_idx], kernel[kr_idx]);
			(*oc) = PADD(tmp, (*oc));
		}
	}

}

		
static inline void conv_d(double *oc, double *ic, double *kernel,
			  int r, int c, int s, int e, int ic_rows, int ic_cols, int k_dim)
{
	int ic_idx, kr_idx;

	for (int k_row = s; k_row < e; k_row++)
	{
		for (int k_col = s; k_col < e; k_col++)
		{
			
			if ((r + k_row < 0) || (r + k_row >= ic_rows) ||
			    (c + k_col < 0) || (c + k_col >= ic_cols))
				continue;
			
			ic_idx = (r+k_row)*ic_cols + (c+k_col);
			kr_idx = (k_row+(k_dim/2))*k_dim + (k_col+(k_dim/2));

			(*oc) += ic[ic_idx]*kernel[kr_idx];
		}
	}

}

void masked_dotprod(share *c, share *a, share *b, int n);
void masked_dotprod2d(share *c, share *a, share *b, int n);
void masked_conv(share *oc, share *ic, share *kernel, int r, int c, int s, int e, int ic_rows, int ic_cols, int k_dim);

	
static inline void add_d(double *c, double *a, double *b)
{ (*c) =  (*a) + (*b); }

static inline void add_f(fixed_point_t *c, fixed_point_t *a, fixed_point_t *b)
{ (*c) =  PADD((*a), (*b)); }

void masked_add(share *c, share *a, share *b);


static inline void asg_d(double *c, double *b)
{ (*c) = (*b); }

static inline void asg_f(fixed_point_t *c, fixed_point_t *b)
{ (*c) = (*b); }

void masked_mov(share *b, share *a);


static inline void cmp_d(double *c, double *d, double *a, double *b)
{
	(*c) = (*a) > (*b);
	(*d) = (*a) <= (*b);
}

static inline void cmp_f(fixed_point_t *c, fixed_point_t *d, fixed_point_t *a, fixed_point_t *b)
{
	(*c) = PGT((*a),(*b));
	(*d) = PLE((*a),(*b));
}

void masked_cmp(share *c, share *d, share *a, share *b);

static inline void mvi_d(double *b, int a)
{ (*b) = (double)a; }

static inline void mvi_f(fixed_point_t *b, int a)
{ (*b) = (fixed_point_t)a; }

static inline void masked_mvi(share *b, int a)
{

	b->s[1] = a;
	b->s[0] = PLSHIFT(1, 31);
	b->s[1] = PADD(b->s[1], - b->s[0]);
	return;
}


static inline void zero_a_d(double *a, int size)
{
	for (int i = 0; i < size; i++)
		a[i] = 0;
}


static inline void zero_a_f(fixed_point_t *a, int size)
{
	for (int i = 0; i < size; i++)
		a[i] = 0;
}

static inline void zero_a_s(share *a, int size)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < D; j++)
			a[i].s[j] = 0;

}


static inline void trunc_f(fixed_point_t *a)
{ *(a) = PRSHIFT((*(a)), FP_FRACTIONAL_BITS); }

static inline void trunc_d(double *a)
{ return; }

void masked_trunc_sni(share *a);


ring_t rand_ring_t();
extern int rnd_cnt;
extern int L;
extern int PROFILE_RND;
void set_PROFILE_RNDCNT_ON();
void set_PROFILE_RNDCNT_OFF();
void set_PRNG_ON()    ;
void set_PRNG_OFF()   ;
void set_REUSE_ON(int nm)   ;
void set_REUSE_OFF()  ;
int get_PRNG_status() ;
int get_REUSE_status();
void reset_PRNG();
void print_primop_cnt();
void reset_primop_cnt();
#endif
