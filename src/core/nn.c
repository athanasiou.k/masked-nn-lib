#include "nn.h"

#ifdef T
#undef T
#endif
#define T double
#include "nn_templates.c"

#ifdef T
#undef T
#endif
#define T fixed_point_t
#include "nn_templates.c"

#ifdef T
#undef T
#endif
#define T share
#include "nn_templates.c"

#define NUM_MASKS 100

int error_cnt = 0;
int PRNG_ON = 0;
int REUSE_ON = 0;
int PROFILE_RND = 0;
int num_masks;
int pmulc = 0;
int paddc = 0;
int pxorc = 0;
int pandc = 0;
int prshc = 0;
int pcmpc = 0;
int pnegc = 0;
int rnd_cnt = 0;
int L=0;
ring_t masks[NUM_MASKS];

void set_PROFILE_RNDCNT_ON()  {PROFILE_RND = 1;}
void set_PROFILE_RNDCNT_OFF() {PROFILE_RND = 0;}
void print_primop_cnt(){
	printf("PADD:%d\t",paddc);
	printf("PMUL:%d\t",pmulc);
	printf("PXOR:%d\t",pxorc);
	printf("PLSH:%d\t",prshc);
	printf("PAND:%d\t",pandc);
	printf("PCMP:%d\t",pcmpc);
	printf("PNEG:%d\t",pnegc);
	
}
void reset_primop_cnt(){
	paddc = 0;
	pmulc = 0;
	pxorc = 0;
	prshc = 0;
	pandc = 0;
	pcmpc = 0;
	pnegc = 0;
}
void set_PRNG_ON() { PRNG_ON = 1;}
void set_PRNG_OFF() { PRNG_ON = 0;}
void set_REUSE_ON(int nm)
{
	REUSE_ON = 1;
	num_masks = nm;
	if (nm> NUM_MASKS)
		printf("Insufficient mask cache size.");
	
}
void set_REUSE_OFF() { REUSE_ON = 0; L=0;}
void reset_PRNG()
{
	reset_Keccak();
	rnd_cnt=0;
	if (REUSE_ON)
	{
		L=0;
		for (int i = 0; i < num_masks; i++)
			masks[i] = rand_ring_t();
		L=1;
	}
	else
		L=0;
	
}

int get_PRNG_status() {return PRNG_ON;}
int get_REUSE_status() {return REUSE_ON;}

ring_t rand_ring_t()
{
	if (!PRNG_ON) return 1<<31;
	if (L > 0 && REUSE_ON)
	{

		return masks[L];
	}
	else
	{
		rnd_cnt++;
		ring_t t = 0;
		for(int i = 0; i < (sizeof(ring_t)/sizeof(unsigned char)); i++)
		{
			uint8_t r = RetRand();
			t |= r << (8*i);
		}

		if ((t < FP_SAFE_UB_NUM) ||
		    (t > FP_SAFE_LB_NUM))
		{
			error_cnt++;
		}

		return t;
	}
}

void mask_a(fixed_point_t *a, int size, share *a_m)
{
	for (int i=0; i < size; i++)
	{
		a_m[i].s[D-1] = a[i];
		for (int j=0; j < D-1; j++)
		{
			a_m[i].s[j] = rand_ring_t();
			a_m[i].s[D-1] = (ring_t)(a_m[i].s[D-1] - a_m[i].s[j]);
		}
	}
}

void masked_mul2d(share *c, share *a, share *b)
{

	ring_t rnd, tmp_a, tmp_b, acc0, acc1;
	rnd = rand_ring_t();
	acc0 = rnd;
	acc1 = -rnd;
	
	tmp_a = PMUL(a[0].s[0], b[0].s[1]);
	tmp_b = PMUL(a[0].s[1], b[0].s[0]);
	acc0 = PADD(acc0, tmp_a);
	acc0 = PADD(acc0, tmp_b);
	
	tmp_a = PMUL(a[0].s[0], b[0].s[0]);
	tmp_b = PMUL(a[0].s[1], b[0].s[1]);
	acc1 = PADD(acc1, tmp_a);
	acc1 = PADD(acc1, tmp_b);

	c[0].s[0] = acc0;
	c[0].s[1] = acc1;
	L++;

}

// c=a*b
void masked_dotprod2d(share *c, share *a, share *b, int n)
{

	ring_t rnd, tmp_a, tmp_b;;

	rnd = rand_ring_t();
	c->s[0] = rnd;
	c->s[1] = -rnd;

	for (int k = 0; k < n ; k++)
	{

		tmp_a = PMUL(a[k].s[0], b[k].s[1]);
		tmp_b = PMUL(a[k].s[1], b[k].s[0]);
		c->s[0] = PADD(c->s[0], tmp_a);
		c->s[0] = PADD(c->s[0], tmp_b);
		
		tmp_a = PMUL(a[k].s[0], b[k].s[0]);
		tmp_b = PMUL(a[k].s[1], b[k].s[1]);
		c->s[1] = PADD(c->s[1], tmp_a);
		c->s[1] = PADD(c->s[1], tmp_b);
	}


}

void masked_conv(share *oc, share *ic, share *kernel, int r, int c, int s, int e, int ic_rows, int ic_cols, int k_dim)
{

	oc->s[0] = rand_ring_t();
	oc->s[1] = -oc->s[0];
	ring_t tmp_a, tmp_b;
	int ic_idx, kr_idx;

	for (int k_row = s; k_row < e; k_row++)
	{
		for (int k_col = s; k_col < e; k_col++)
		{

			if ((r + k_row < 0) || (r + k_row >= ic_rows) ||
			    (c + k_col < 0) || (c + k_col >= ic_cols))
				continue;

			
			ic_idx = (r+k_row)*ic_cols + (c+k_col);
			kr_idx = (k_row+(k_dim/2))*k_dim + (k_col+(k_dim/2));

			
			tmp_a = PMUL(ic[ic_idx].s[0], kernel[kr_idx].s[1]);
			tmp_b = PMUL(ic[ic_idx].s[1], kernel[kr_idx].s[0]);
			oc->s[0] = PADD(oc->s[0], tmp_a);
			oc->s[0] = PADD(oc->s[0], tmp_b);
			
			tmp_a = PMUL(ic[ic_idx].s[0], kernel[kr_idx].s[0]);
			tmp_b = PMUL(ic[ic_idx].s[1], kernel[kr_idx].s[1]);
			oc->s[1] = PADD(oc->s[1], tmp_a);
			oc->s[1] = PADD(oc->s[1], tmp_b);
		}
	}
}

void masked_add(share *c, share *a, share *b)
{
	for (int i=0; i<D; i++)
	{
		c->s[i]=PADD(a->s[i], b->s[i]);
	}
}

// c = a - b
void masked_sub(share *c, share *a, share *b)
{
	for (int i=0; i<D; i++)
	{
		c->s[i]=(ring_t)PADD(a->s[i], (-b->s[i]));
	}
}

//b:=a
void masked_mov(share *b, share *a)
{
	for (int i=0; i<D; i++)
		b->s[i] = a->s[i];

}


void arithmetic_to_boolean_sni(share *a, share *b)
{
	//A:  a[1]
	//r:  a[0]
	//x': b[1]
	//r:  b[0]

	ring_t tau, gamma, omega, r;
	int8_t i;

	r = rand_ring_t();
	L++;
	//For 1-sni-ness
	a->s[0] = PADD(a->s[0], r);
	a->s[1] = PADD(a->s[1], -r);


	gamma = rand_ring_t();
	L++;
	tau = PMUL(2, gamma);
	b->s[1] = PXOR(gamma, a->s[0]);
	omega = PAND(gamma, b->s[1]);
	b->s[1] = PXOR(tau, a->s[1]);
	gamma = PXOR(gamma, b->s[1]);
	gamma = PAND(gamma, a->s[0]);
	omega = PXOR(omega, gamma);
	gamma = PAND(tau, a->s[1]);
	omega = PXOR(omega, gamma);
	for (i=1; i < RING_BITS; i++)
	{
		gamma = PAND(tau, a->s[0]);
		gamma = PXOR(gamma, omega);
		tau = PAND(tau, a->s[1]);
		gamma = PXOR(gamma, tau);
		tau = PMUL(2, gamma);
	}
	b->s[1] = PXOR(b->s[1], tau);
	b->s[0] = a->s[0];
}

void boolean_to_arithmetic_sni(share *b, share *a)
{

	//x': b[1]
	//r : b[0]
	//A : a[1]
	//r : a[0]
	ring_t tau, gamma, r;

	//For 1-sni-ness
	r = rand_ring_t();
	L++;
	b->s[0] = PXOR(b->s[0], r);
	b->s[1] = PXOR(b->s[1], r);

	gamma = rand_ring_t();
	L++;
	tau = PXOR(b->s[1], gamma);
	tau = PADD(tau, -gamma);
	tau = PXOR(tau, b->s[1]);
	gamma = PXOR(gamma, b->s[0]);
	a->s[1] = PXOR(b->s[1], gamma);
	a->s[1] = PADD(a->s[1], -gamma);
	a->s[1] = PXOR(a->s[1], tau);
	a->s[0] = b->s[0];
}

void masked_cmp(share* c, share* d, share *a, share *b)
{
	share t[1], boolean_shares[1], msb_shares[1];
	
	masked_sub(t, a, b);

	arithmetic_to_boolean_sni(t, boolean_shares);
	for (int k = 0; k < D; k++)
		msb_shares[0].s[k] = PRSHIFT(((boolean_shares[0].s[k])& 0x80000000), 31);


	// d = (a < b) 
	boolean_to_arithmetic_sni(msb_shares, d);
	L -= 2;
	
	// c = (a > b)
	msb_shares->s[D-1] = PXOR(msb_shares[0].s[D-1], 1);
	boolean_to_arithmetic_sni(msb_shares, c);

}


void masked_trunc_sni(share *a)
{
	ring_t r;
	r = rand_ring_t();

	a->s[0] = PRSHIFT(a->s[0], FP_FRACTIONAL_BITS);
	ring_t tmp = PRSHIFT((-(a->s[1])), FP_FRACTIONAL_BITS);
	a->s[1] = PADD(RING_MAX_NUM, - tmp);

	//For 1-sni-ness
	a->s[0] = PADD(a->s[0], r);
	a->s[1] = PADD(a->s[1], -r);
}
