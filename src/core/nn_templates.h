#ifdef T

#include "templates.h"

void TEMPLATE(dense,T)(T *in, int in_dim, T *w, int out_dim, T *out, T *bias, int activation);
void TEMPLATE(relu,T)(T* v, int size);
void TEMPLATE(maxpool,T)(T *f, int f_rows, int f_cols, int k_dim, T *d, int d_rows, int d_cols);
void TEMPLATE(argmax,T)(T* v, int size, T* max, T* arg);
void TEMPLATE(maxpoollayer,T)(T *in, int in_channels, int in_dim, int k_dim, int stride, int padding, T *out);
void TEMPLATE(convlayer,T)(T *in, int in_channels, int in_dim,
			   T *weights, int kernel_dim, int stride, int padding,
			   T *out, int out_channels, T *bias);
void TEMPLATE(print_cnn_layer,T)(T *l, int channels, int dim, int kernel_dim);
void TEMPLATE(print_fnn_layer,T)(T *l, int dim);




#endif
