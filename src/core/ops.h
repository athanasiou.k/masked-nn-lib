#ifndef OPS_H_

#define MUL(X, Y, Z) _Generic((X),			\
			      double* : mul_d,		\
			      fixed_point_t*: mul_f,	\
			      share*: masked_mul2d	\
		)(X, Y, Z)

#define DOT(X, Y, Z, W) _Generic((X),					\
				 double* : dotprod_d,			\
				 fixed_point_t*: dotprod_f,		\
				 share*: masked_dotprod2d		\
		)(X, Y, Z, W)

#define CONV(X, Y, Z, W, A, B, C, D, E, F) _Generic((X),		\
						    double* : conv_d,	\
						    fixed_point_t*: conv_f, \
						    share*: masked_conv	\
		)(X, Y, Z, W, A, B, C, D, E, F)

#define ADD(X, Y, Z) _Generic((X),			\
			      double* : add_d,		\
			      fixed_point_t*: add_f,	\
			      share*: masked_add	\
		)(X, Y, Z)



#define ASG(X, Y) _Generic((X),				\
			   double* : asg_d,		\
			   fixed_point_t*: asg_f,	\
			   share*: masked_mov		\
		)(X, Y)

#define CMP(W, X, Y, Z) _Generic((W),			\
			      double* : cmp_d,		\
			      fixed_point_t* : cmp_f,	\
			      share* : masked_cmp	\
		)(W, X, Y, Z)



#define NEG(X, Y) _Generic((X),				\
			   double* : neg_d,		\
			   fixed_point_t*: neg_f,	\
			   share*: masked_neg		\
		)(X, Y)

#define MVI(X, Y) _Generic((X),				\
			   double* : mvi_d,		\
			   fixed_point_t*: mvi_f,	\
			   share*: masked_mvi		\
		)(X, Y)

#define TRN(X) _Generic((X),				\
			   double* : trunc_d,		\
			   fixed_point_t*: trunc_f,	\
			   share*: masked_trunc_sni	\
		)(X)

#define PRINT(X) _Generic((X),				\
			  double*: print_d,		\
			  share*: print_r,		\
			  fixed_point_t*: print_f	\
		)(X)


#define ZERO(X, Y) _Generic((X),			\
			    double*: zero_a_d,		\
			    fixed_point_t*: zero_a_f,	\
			    share* :zero_a_s		\
		)(X, Y)

#define ARRAY_SIZE(a) (sizeof (a) / sizeof *(a))

#define RSHIFT(x, y) ( (x) >> (y) )

#endif
