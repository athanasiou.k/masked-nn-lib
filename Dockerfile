FROM python:3.6-buster
WORKDIR /usr/src/
COPY src/ .
ADD mnist-test-data/ /usr/mnist-test-data
RUN apt-get update && \
    apt-get -y --no-install-recommends install gcc-8 make && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 100 && \ 
    rm -rf /var/lib/apt/lists/* # remove cached apt files
RUN make
ENV LD_LIBRARY_PATH="/usr/src"
CMD ["bash", "-c", "make run-u && make run-s && python stats.py"]
