# Masked NN Library

A C library for NN inference provably secure against Differential Power
Analysis. The library is described in the Paper "Masking Feedforward Neural
Networks Against Power Analysis Attacks" appearing in the 22nd Privacy Enhancing
Technologies Symposium (PETS 2022).

The repository contains the sources of the library, as well as trained inference
NN models. Following the Quick start or Docker instructions below, reproduces
the results described in the section "Performance Evaluation" of the paper.

## Quick start
The following have been tested on x86 machines running Linux.
### Requirements
- gcc-8.3 or later (also tested on gcc-11.1.0)
- GNU Make
- GNU bash
- python3

### Building from sources and running locally
1. Clone the repository.

2. Enter the source code directory `src`:

```
% cd src
```

3. Compile the masked-nn library.This will produce the shared library
`libmasked-nn-lib.so` under the `src` directory.

```
% make lib
```

4. Compile the fixed-point (unmasked/unshared) and masked/shared implementations
for all the NN models. NN models are named as `{f|m}{type}{arch}` where `f`
denotes fixed-point, `m` denotes masked. For example, `fmlp128-128` is the
binary of a fixed-point Multi-Layer Perceptron with 2 hidden layers, 128 neurons
each. The binaries for the models will be produced under the `src` directory.

```
% make models
```
	
5. Run the unshared and shared binaries. The shared binaries will be executed
under all three modes, namely PRNG_OFF, PRNG_ON-ORIGINAL,
PRNG_OFF-TIGHTENED. Execution of each model, depending on the model's size/type
and the mode it's executed on, will require from some seconds up to a couple of
minutes. The following Makefile rules automatically export the library path
environment variable.

```
% make run-u
% make run-s
```

6. Consolidate the runtime reports in JSON and LaTeX-table format.
```
% python stats.py
```

## Docker

The performance experiments can also be replicated using a Docker container.

1. Clone the repository.

2. From the root of the repository build the docker
image. In addition to building the image, this will compile all NN models.

```
docker build . -t masked-nn
```

3. Execute the unshared and shared binaries. Collect performance evaluation results and print them in
JSON and LaTeX-table format.

```
docker run masked-nn
```

4. Alternatively, after building the image, you can directly run the Quick Start
steps in the container by attaching an interactive tty to the container with:

```
docker run -it masked-nn sh
```	

### Requirements
- Docker 20.10.8

## Models

| Type | Architecture | NN Files |
|-|-|-|
|MLP|15| `fmlp.c`, `mmlp.c`|
|MLP|1000| `fmlp1000.c`, `mmlp1000.c`|
|MLP|128-128| `fmlp128-128.c`, `mmlp128-128.c`|
|MLP|300-100| `fmlp300-100.c`, `mmlp300-100.c`|
|MLP|128-128-128| `fmlp128-128-128.c`, `mmlp128-128-128.c`|
|MLP|256-256-25| `fmlp256-256-256.c`, `mmlp256-256-256.c`|
|BNN|512| `fbnn512.c`, `mbnn512.c`|
|CNN|(6,5)-(16,5)-256-120-84| `flenet.c`, `mlenet.c`|
|CNN|(16,5)-(32,5)-1568| `fccn.c`. `mccn.c`|

